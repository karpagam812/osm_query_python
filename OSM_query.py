import pandas as pd
import requests
import json
from geopy.exc import GeocoderTimedOut
import geopy
import math
import requests
import json
import ast
from geopy.geocoders import Nominatim




#Round 1 -- filling lat, long
def find_geocode(address):
    url = 'http://photon.komoot.de/api/?q='
    print(address)
    resp = requests.get(url=url+address)
    data = json.loads(resp.text)
    #print(data['features'])
    if('features' in data.keys()):
        if(not data['features']):
            print(address," not found")
            coord = '0,0'
        else:
            long,lat = data['features'][0]['geometry']['coordinates']
            coord = str(lat) + ',' + str(long)
            print(coord)
    else:
        print("not found features")
        coord = '0,0'
    return(coord)





#round 2 filling lat, long

def do_geocode(address, attempt=1, max_attempts=50):
    try:
        geolocator = Nominatim()
        return geolocator.geocode(address)
    except GeocoderTimedOut:
        if attempt <= max_attempts:
            print("i am here")
            return do_geocode(address, attempt=attempt+1)
        raise

def geol(address,flag):
    if(flag == "0,0"):
        location = do_geocode(address)
        if(location is None):
            coord = "0,0"
        else:
            print("Got lat long")
            coord = str(location.latitude) + "," + str(location.longitude)
        return(coord)
    else:
        return(flag)


#============================================BOUNDING BOX==============================

#Overpass API query

class BoundingBox(object):
    def __init__(self, *args, **kwargs):
        self.lat_min = None
        self.lon_min = None
        self.lat_max = None
        self.lon_max = None


def get_bounding_box(latitude_in_degrees, longitude_in_degrees, half_side_in_km):
#    assert half_side_in_miles > 0
    assert latitude_in_degrees >= -90.0 and latitude_in_degrees  <= 90.0
    assert longitude_in_degrees >= -180.0 and longitude_in_degrees <= 180.0

#    half_side_in_km = half_side_in_miles * 1.609344
    lat = math.radians(latitude_in_degrees)
    lon = math.radians(longitude_in_degrees)

    radius  = 6371
    # Radius of the parallel at given latitude
    parallel_radius = radius*math.cos(lat)

    lat_min = lat - half_side_in_km/radius
    lat_max = lat + half_side_in_km/radius
    lon_min = lon - half_side_in_km/parallel_radius
    lon_max = lon + half_side_in_km/parallel_radius
    rad2deg = math.degrees

    box = BoundingBox()
    box.lat_min = rad2deg(lat_min)
    box.lon_min = rad2deg(lon_min)
    box.lat_max = rad2deg(lat_max)
    box.lon_max = rad2deg(lon_max)

    return (box)

def get_nbox(lat,lon,N):
    b = get_bounding_box(float(lat),float(lon),float(N))
    slat = round(b.lat_min,3)
    nlat = round(b.lat_max,3)
    slon = round(b.lon_min,3)
    nlon = round(b.lon_max,3)

    box = str(slat) + "," + str(slon) + "," + str(nlat) + "," + str(nlon)
    nbox = """node(""" + box + """)"""
    return(nbox)
#=========================================RESTAURANT=========================
def get_restaurants_count(nbox):
    #OSM Query
    overpass_url = "http://overpass-api.de/api/interpreter"
    overpass_query = """[out:json];
    """ + nbox + """
    [amenity=restaurant];
    out;"""
    response = requests.get(overpass_url, 
                            params={'data': overpass_query})
    print(response)
    if response.status_code == 200:
        data = response.json()
        no_of_restaurants = len(data['elements'])
        print(no_of_restaurants)
    else:
        data = 0
        no_of_restaurants="Bad query"
    return(no_of_restaurants)


def find_rest(lat,lon):
    if(lat == "0"):
        return("Invalid")
    N = 1
    nbox = get_nbox(lat,lon,N)
    rest_count = get_restaurants_count(nbox)
    return(rest_count)
#===================================================BUSSTOP===================================
def get_busstop(lat, lon, N):
    nbox = get_nbox(lat, lon, N)
    print(nbox)
    #OSM Query
    overpass_url = "http://overpass-api.de/api/interpreter"
    overpass_query = """[out:json];
    """ + nbox + """
    [highway=bus_stop];
    out;"""
    response = requests.get(overpass_url, 
                            params={'data': overpass_query})
    print(response)
    if response.status_code == 200:
        data = response.json()
        return(data['elements'])
    else:
        data = 0
        res="Bad query"
        return(res)
    
def find_bus(lat,lon):
    print("Latitude=================================",lat)
    if(lat == "0"):
        return("Invalid")
    N = 0.1
    r1 = get_busstop(lat,lon, N)
    while(len(r1) == 0):
        N = N + 1
        print(N)
        r1 = get_busstop(lat,lon,N)
    name = " "
    lat = " "
    lon = " "
    if("tags" in r1[0]):
        if("name" in r1[0]['tags']):
            name = r1[0]['tags']['name']
    if("lat" in r1[0]):
        lat = r1[0]['lat']
    if("lon" in r1[0]):
        lon = r1[0]['lon']
    bus_info = str(name) + ',' + str(lat) + ',' + str(lon)
    print(bus_info)
    return(bus_info)


#=========================================================SCHOOL=================================
def get_school(lat, lon, N):
    nbox = get_nbox(lat, lon, N)
    #print(nbox)
    #OSM Query
    overpass_url = "http://overpass-api.de/api/interpreter"
    overpass_query = """[out:json];
    """ + nbox + """
    [amenity=school];
    out;"""
    response = requests.get(overpass_url, 
                            params={'data': overpass_query})
    #print(response)
    if response.status_code == 200:
        data = response.json()
        return(data['elements'])
    else:
        data = 0
        res="Bad query"
        return(res)
    
def find_school(lat,lon):
    print("Latitude=================================",lat)
    if(lat == "0"):
        return("Invalid")
    N = 1
    r1 = get_school(lat,lon, N)
    while(len(r1) == 0):
        N = N + 1
        print(N)
        r1 = get_school(lat,lon,N)
    name = " "
    lat = " "
    lon = " "
    if("tags" in r1[0]):
        if("name" in r1[0]['tags']):
            name = r1[0]['tags']['name']
    if("lat" in r1[0]):
        lat = r1[0]['lat']
    if("lon" in r1[0]):
        lon = r1[0]['lon']
    sch_info = str(name) + ',' + str(lat) + ',' + str(lon)
    print(sch_info)
    return(r1)

#=============================================METRO========================================
def get_metro(lat, lon, N):
    nbox = get_nbox(lat, lon, N)
    print(nbox)
    #OSM Query
    overpass_url = "http://overpass-api.de/api/interpreter"
    overpass_query = """[out:json];
    """ + nbox + """
    [railway=station];
    out;"""
    response = requests.get(overpass_url, 
                            params={'data': overpass_query})
    print(response.status_code)
    if response.status_code == 200:
        data = response.json()
        return(data['elements'])
    else:
        data = 0
        res="Bad query"
        return(res)
    
def find_metro(lat,lon):
    print("Latitude=================================",lat)
    if(lat == "0"):
        return("Invalid")
    N = 1
    r1 = get_metro(lat,lon, N)
    print(r1)
    while(len(r1) == 0):
        N = N + 3
        print(N)
        r1 = get_metro(lat,lon,N)
    name = " "
    lat = " "
    lon = " "
    print(r1)
    if("tags" in r1[0]):
        if("name" in r1[0]['tags']):
            name = r1[0]['tags']['name']
    if("lat" in r1[0]):
        lat = r1[0]['lat']
    if("lon" in r1[0]):
        lon = r1[0]['lon']
    metro_info = str(name) + ',' + str(lat) + ',' + str(lon)
    print(metro_info)
    return(metro_info)

#=========================================ELEVATION========================================================
    
def get_ele(lat, lon, N):
    nbox = get_nbox(lat, lon, N)
    #print(nbox)
    #OSM Query
    overpass_url = "http://overpass-api.de/api/interpreter"
    overpass_query = """[out:json];
    """ + nbox + """
    [ele];
    out;"""
    response = requests.get(overpass_url, 
                            params={'data': overpass_query})
    #print(response)
    if response.status_code == 200:
        data = response.json()
        return(data['elements'])
    else:
        data = 0
        res="Bad query"
        return(res)
    
def find_ele(lat,lon):
    print("Latitude=================================",lat)
    if(lat == "0"):
        return("Invalid")
    N = 1
    r1 = get_ele(lat,lon, N)
    while(len(r1) == 0):
        N = N + 1
        print(N)
        r1 = get_ele(lat,lon,N)
    ele = " "
    print(r1[0])
    if("tags" in r1[0]):
        if("ele" in r1[0]['tags']):
            ele = r1[0]['tags']['ele']
    print(ele)
    return(ele)



#========================================PARK===========================
    
def get_park(lat, lon, N):
    nbox = get_nbox(lat, lon, N)
    print(nbox)
    #OSM Query
    overpass_url = "http://overpass-api.de/api/interpreter"
    overpass_query = """[out:json];
    """ + nbox + """
    [leisure=park];
    out;"""
    response = requests.get(overpass_url, 
                            params={'data': overpass_query})
    #print(response)
    if response.status_code == 200:
        data = response.json()
        return(data['elements'])
    else:
        data = 0
        res="Bad query"
        return(res)

def find_park(lat,lon):
    print("Latitude=================================",lat)
    if(lat == "0"):
        return("Invalid")
    N = 1
    r1 = get_park(lat,lon, N)
    print(r1)
    while(len(r1) == 0):
        N = N + 7
        print(N)
        r1 = get_park(lat,lon,N)
    name = " "
    lat = " "
    lon = " "
    if("tags" in r1[0]):
        if("name" in r1[0]['tags']):
            name = r1[0]['tags']['name']
    if("lat" in r1[0]):
        lat = r1[0]['lat']
    if("lon" in r1[0]):
        lon = r1[0]['lon']
    park_info = str(name) + ',' + str(lat) + ',' + str(lon)
    print(park_info)
    return(park_info)

#======================================================================================

#school processing
def find_name(a):
    if(a == 'Invalid'):
        a = "Not Found"
        return(a)
    if(a == 'Bad query'):
        a = "Not Found"
        return(a)
    if("tags" in a[0]):
        if("name" in a[0]['tags']):
            return(a[0]['tags']['name'])
    else:
        return(a)


def find_lat(a):
    if(a == 'Invalid'):
        a = "Not Found"
        return(a)
    if(a == 'Bad query'):
        a = "Not Found"
        return(a)
    if("lat" in a[0]):
        return(a[0]['lat'])
    else:
        return(a)


def find_lon(a):
    if(a == 'Invalid'):
        a = "Not Found"
        return(a)
    if(a == 'Bad query'):
        a = "Not Found"
        return(a)
    if("lon" in a[0]):
        return(a[0]['lon'])
    else:
        return(a)

#bus processing
def split_values(a,f):
    if(a == 'Invalid'):
        a = "Not Found"
        return(a)
    if(a == 'Bad query'):
        a = "Not Found"
        return(a)
    a1,a2,a3 = a.split(',')
    if(f == 1):
        return(a1)
    if(f == 2):
        return(a2)
    else:
        return(a3)


    
df = pd.read_csv("Example.csv")
#Finding lat,lon
#round1
df['coord'] = df.apply (lambda row: find_geocode(row['Adresse']), axis=1)

#round 2
df['coord'] = df.apply(lambda row: geol(row['Adresse'],row['coord']), axis = 1)

#spliting lat,lon
df1 = pd.DataFrame(df.coord.str.split(',',1).tolist(), columns = ['lat','lon'])
df1['Adresse'] = df['Adresse']

#Finding other stuffs
df1['no_of_restaurants'] = df1.apply(lambda row: find_rest(row['lat'],row['lon']), axis=1)
df1['metro_info'] = df1.apply(lambda row: find_metro(row['lat'],row['lon']), axis=1)
df1['sch_info'] = df1.apply(lambda row: find_school(row['lat'],row['lon']), axis=1)
df1['bus_info'] = df1.apply(lambda row: find_bus(row['lat'],row['lon']), axis=1)
df1['park_info'] = df1.apply(lambda row: find_park(row['lat'],row['lon']), axis=1)
df1['ele'] = df1.apply(lambda row: find_ele(row['lat'],row['lon']), axis=1)

#processing
df1['school_name'] = df1.apply(lambda row: find_name(row['sch_info']), axis=1)
df1['school_lat'] = df1.apply(lambda row: find_lat(row['sch_info']), axis=1)  
df1['school_lon'] = df1.apply(lambda row: find_lon(row['sch_info']), axis=1)

df1['bus_name'] = df1.apply(lambda row: split_values(row['bus_info'],1), axis=1)
df1['bus_lat'] = df1.apply(lambda row: split_values(row['bus_info'],2), axis=1)
df1['bus_lon'] = df1.apply(lambda row: split_values(row['bus_info'],3), axis=1)

df1['metro_name'] = df1.apply(lambda row: split_values(row['metro_info'],1), axis=1)
df1['metro_lat'] = df1.apply(lambda row: split_values(row['metro_info'],2), axis=1)
df1['metro_lon'] = df1.apply(lambda row: split_values(row['metro_info'],3), axis=1)

df1['park_lat'] = df1.apply(lambda row: split_values(row['park_info'],2), axis=1)
df1['park_lon'] = df1.apply(lambda row: split_values(row['park_info'],3), axis=1)


df1.to_csv("complete_output.csv", index=None)

df2 = df1[[ 'Adresse','lat', 'lon', 'metro_name', 'metro_lat', 'metro_lon','bus_name', 'bus_lat',
       'bus_lon', 'school_name', 'school_lat', 'school_lon','park_lat', 'park_lon','ele','no_of_restaurants']]

df2.to_csv("output.csv", index=None)